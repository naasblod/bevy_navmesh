default:
	cargo build
example:
	cargo run --example debug_astar
clean:
	rm -rf target

.PHONY: all
