use bevy::prelude::*;
use bevy::{render::mesh::Indices, utils::Instant};
use bevy_navmesh::{AppState, MyNavMeshPlugin, NavMesh, Triangle};
use petgraph::algo::astar;
use petgraph::prelude::*;
use rand::thread_rng;
use rand::Rng;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(MyNavMeshPlugin)
        .add_startup_system(setup)
        .add_system_set(SystemSet::on_enter(AppState::Example).with_system(generate_debug_mesh))
        .add_system_set(SystemSet::on_update(AppState::Example).with_system(generate_new_path))
        .run();
}

fn setup(mut commands: Commands) {
    commands
        .spawn_bundle(Camera3dBundle::default())
        .insert_bundle(TransformBundle::from_transform(
            Transform::from_xyz(0.0, 25.0, 0.1).looking_at(Vec3::ZERO, Vec3::Y),
        ));


    commands.insert_resource(AmbientLight {
        color: Color::WHITE,
        brightness: 1.02,
    });

}
fn generate_debug_mesh(
    mut commands: Commands,
    nav_mesh: Res<NavMesh>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let mut rng = thread_rng();
    let graph = &nav_mesh.0;
    for node in graph.node_indices() {
        let r: f32 = rng.gen();
        let g: f32 = rng.gen();
        let b: f32 = rng.gen();
        commands
            .spawn_bundle(PbrBundle {
                mesh: meshes.add(triangle_to_mesh(&graph.node_weight(node).unwrap().triangle)),
                material: materials.add(Color::rgb(r, g, b).into()),
                ..default()
            })
            .insert_bundle(SpatialBundle::default());
    }
}

fn triangle_to_mesh(triangle: &Triangle) -> Mesh {
    let mut mesh = Mesh::new(bevy::render::render_resource::PrimitiveTopology::TriangleList);
    mesh.set_indices(Some(Indices::U32(vec![0, 1, 2])));

    let vert_pos = vec![
        triangle.positions.get(0).unwrap().clone(),
        triangle.positions.get(1).unwrap().clone(),
        triangle.positions.get(2).unwrap().clone(),
    ];

    mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, vert_pos);
    mesh.insert_attribute(
        Mesh::ATTRIBUTE_NORMAL,
        vec![
            triangle.normals.get(0).unwrap().clone(),
            triangle.normals.get(1).unwrap().clone(),
            triangle.normals.get(2).unwrap().clone(),
        ],
    );
    mesh.insert_attribute(
        Mesh::ATTRIBUTE_UV_0,
        vec![[1.0, 1.0], [1.0, 1.0], [1.0, 1.0]],
    );
    mesh
}

fn generate_new_path(
    mut commands: Commands,
    keyboard_input: Res<Input<KeyCode>>,
    nav_mesh: Res<NavMesh>,
    mut path_entities: Local<Vec<Entity>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    if keyboard_input.just_released(KeyCode::Space) {
        for mesh in &*path_entities {
            commands.entity(*mesh).despawn_recursive();
        }
        path_entities.clear();
        let node_count = nav_mesh.0.node_count();
        let start: usize = rand::thread_rng().gen_range(0..node_count);
        let end: usize = rand::thread_rng().gen_range(0..node_count);
        let start_index: NodeIndex = NodeIndex::new(start);
        let end_index: NodeIndex = NodeIndex::new(end);
        let start = Instant::now();
        let path = astar(
            &nav_mesh.0,
            start_index,
            |finish| finish == end_index,
            |e| *e.weight(),
            |_| 0.0,
        );

        info!("path generation took: {:?}", start.elapsed());

        if let Some(path) = path {
            for node_index in path.1 {
                let handle = commands
                    .spawn_bundle(PbrBundle {
                        mesh: meshes.add(Mesh::from(shape::Cube::new(0.5))),
                        material: materials.add(if node_index == start_index {
                            Color::GREEN.into()
                        } else if node_index == end_index {
                            Color::RED.into()
                        } else {
                            Color::BLACK.into()
                        }),
                        ..default()
                    })
                    .insert_bundle(SpatialBundle::from_transform(Transform::from_translation(
                        nav_mesh
                            .0
                            .node_weight(node_index)
                            .unwrap()
                            .triangle
                            .center
                            .into(),
                    )))
                    .id();
                path_entities.push(handle);
            }
        }
    }
}
