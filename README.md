## bevy_navmesh

Creates a navmesh from a `.gltf` or `.glb` file with a planar mesh with the name "Navmesh"

there's and example `blend` file to help you get started.

### HOW_TO
`make example`
or 
cargo --example debug_astar



### TODO
learn more about asset loading.
move asset loading out of the lib and into the example.
implement some sort of string pulling algorithm so that actors can take the shortest path through triangles.
make it fancy
