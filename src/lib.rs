use bevy::gltf::Gltf;
use bevy::gltf::GltfMesh;
use bevy::gltf::GltfNode;
use bevy::prelude::*;
use bevy_asset_loader::prelude::*;
use petgraph::prelude::*;

pub struct MyNavMeshPlugin;

pub struct NavMeshPath(pub String);

#[derive(AssetCollection)]
pub struct GltfAssets {
    #[asset(path = "navmesh-test.gltf")]
    gltf_med: Handle<Gltf>,
}

#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub enum AppState {
    LoadAssets,
    LoadGltf,
    Example,
}

pub struct NavMesh(pub UnGraph<Node, f32>);

impl Plugin for MyNavMeshPlugin {
    fn build(&self, app: &mut App) {
        app.add_loading_state(
            LoadingState::new(AppState::LoadAssets)
                .continue_to_state(AppState::LoadGltf)
                .with_collection::<GltfAssets>(),
        )
        .add_state(AppState::LoadAssets)
        .add_system_set(SystemSet::on_enter(AppState::LoadGltf).with_system(load_level_gltf))
        .add_system_set(SystemSet::on_update(AppState::LoadGltf).with_system(done_loading));
    }
}

#[derive(Default)]
pub struct Node {
    pub triangle: Triangle,
}

#[derive(Default)]
pub struct Triangle {
    pub positions: [[f32; 3]; 3],
    pub normals: [[f32; 3]; 3],
    pub center: [f32; 3],
    pub triangle_normal: Vec3,
    pub indices: [usize; 3],
}

fn load_level_gltf(
    mut commands: Commands,
    gltf_asset_handle: Res<GltfAssets>,
    gltf_assets: Res<Assets<Gltf>>,
    gltf_nodes: Res<Assets<GltfNode>>,
    gltf_meshes: Res<Assets<GltfMesh>>,
    meshes: Res<Assets<Mesh>>,
) {
    let handle = &gltf_asset_handle.gltf_med;
    info!("handle: {:?}", handle);
    let gltf = gltf_assets.get(handle).unwrap();
    commands
        .spawn_bundle(SceneBundle {
            scene: gltf.scenes[0].clone(),
            ..default()
        })
        .insert_bundle(TransformBundle::from_transform(Transform::from_xyz(
            0.0, -1.0, 0.0,
        )));

    let nav_mesh_handle = gltf.named_nodes.get("Navmesh").unwrap();
    let nav_mesh_node = gltf_nodes.get(nav_mesh_handle).unwrap();

    let nav_mesh = gltf_meshes
        .get(&nav_mesh_node.mesh.clone().unwrap())
        .unwrap();
    let plane = nav_mesh.primitives.get(0).unwrap();

    commands.insert_resource(generate_graph_from_mesh(meshes.get(&plane.mesh).unwrap()));
}

fn done_loading(nav_mesh: Option<Res<NavMesh>>, mut app_state: ResMut<State<AppState>>) {
    if nav_mesh.is_some() {
        app_state.set(AppState::Example).unwrap();
    }
}

pub fn generate_graph_from_mesh(mesh: &Mesh) -> NavMesh {
    let indice_size = mesh.indices().unwrap().len();

    let mut vertex_positions = Vec::<[f32; 3]>::new();
    let mut vertex_normals = Vec::<[f32; 3]>::new();

    for attribute in mesh.attributes() {
        if attribute.0 == Mesh::ATTRIBUTE_POSITION.id {
            let verts = attribute.1;
            for v in verts.as_float3().unwrap() {
                vertex_positions.push(*v);
            }
        } else if attribute.0 == Mesh::ATTRIBUTE_NORMAL.id {
            let verts = attribute.1;
            for normal in verts.as_float3().unwrap() {
                vertex_normals.push(*normal);
            }
        }
    }

    let indice = mesh.indices().unwrap().clone();
    let s: Vec<usize> = indice.iter().collect();

    let mut nav_graph = UnGraph::<Node, f32>::new_undirected();

    for i in (0..indice_size).step_by(3) {
        let i1 = s[i];
        let i2 = s[i + 1];
        let i3 = s[i + 2];

        let vert_pos = [
            *vertex_positions.get(i1).unwrap(),
            *vertex_positions.get(i2).unwrap(),
            *vertex_positions.get(i3).unwrap(),
        ];

        let vert_normals = [
            *vertex_normals.get(i1).unwrap(),
            *vertex_normals.get(i2).unwrap(),
            *vertex_normals.get(i3).unwrap(),
        ];

        let triangle_normal = (Vec3::from(vert_pos[0]) - Vec3::from(vert_pos[1]))
            .cross(Vec3::from(vert_pos[0]) - Vec3::from(vert_pos[2]))
            .normalize_or_zero();

        nav_graph.add_node(Node {
            triangle: Triangle {
                positions: vert_pos,
                center: [
                    (vert_pos[0][0] + vert_pos[1][0] + vert_pos[2][0]) / 3.0,
                    (vert_pos[0][1] + vert_pos[1][1] + vert_pos[2][1]) / 3.0,
                    (vert_pos[0][2] + vert_pos[1][2] + vert_pos[2][2]) / 3.0,
                ],
                triangle_normal,
                indices: [i1, i2, i3],
                normals: vert_normals,
            },
        });
    }

    NavMesh(calculate_neighbours(nav_graph))
}

pub fn generate_triangles_from_bevy_mesh(mesh: &Mesh) -> Vec<Triangle> {
    let mut result = Vec::new();
    let indice_size = mesh.indices().unwrap().len();

    let mut vertex_positions = Vec::<[f32; 3]>::new();
    let mut vertex_normals = Vec::<[f32; 3]>::new();

    for attribute in mesh.attributes() {
        if attribute.0 == Mesh::ATTRIBUTE_POSITION.id {
            let verts = attribute.1;
            for v in verts.as_float3().unwrap() {
                vertex_positions.push(*v);
            }
        } else if attribute.0 == Mesh::ATTRIBUTE_NORMAL.id {
            let verts = attribute.1;
            for normal in verts.as_float3().unwrap() {
                vertex_normals.push(*normal);
            }
        }
    }

    let indice = mesh.indices().unwrap().clone();
    let s: Vec<usize> = indice.iter().collect();

    for i in (0..indice_size).step_by(3) {
        let i1 = s[i];
        let i2 = s[i + 1];
        let i3 = s[i + 2];

        let vert_pos = [
            *vertex_positions.get(i1).unwrap(),
            *vertex_positions.get(i2).unwrap(),
            *vertex_positions.get(i3).unwrap(),
        ];

        let vert_normals = [
            *vertex_normals.get(i1).unwrap(),
            *vertex_normals.get(i2).unwrap(),
            *vertex_normals.get(i3).unwrap(),
        ];

        let triangle_normal = (Vec3::from(vert_pos[0]) - Vec3::from(vert_pos[1]))
            .cross(Vec3::from(vert_pos[0]) - Vec3::from(vert_pos[2]))
            .normalize_or_zero();

        result.push(Triangle {
            positions: vert_pos,
            normals: vert_normals,
            triangle_normal,
            center: [
                (vert_pos[0][0] + vert_pos[1][0] + vert_pos[2][0]) / 3.0,
                (vert_pos[0][1] + vert_pos[1][1] + vert_pos[2][1]) / 3.0,
                (vert_pos[0][2] + vert_pos[1][2] + vert_pos[2][2]) / 3.0,
            ],

            indices: [i1, i2, i3],
        });
    }
    result
}

fn calculate_neighbours(mut graph: UnGraph<Node, f32>) -> UnGraph<Node, f32> {
    let mut edges_to_add: Vec<(NodeIndex, NodeIndex, f32)> = Vec::new();
    for node in graph.node_indices() {
        let a = &graph[node];
        for other in graph.node_indices() {
            if node == other {
                continue;
            }
            let b = &graph[other];
            let mut matching_positions = 0;
            for a_position in a.triangle.positions {
                for b_position in b.triangle.positions {
                    if a_position == b_position {
                        matching_positions += 1;
                    }
                }
                if matching_positions > 1 {
                    let weight =
                        Vec3::from(a.triangle.center).distance(Vec3::from(b.triangle.center));
                    edges_to_add.push((node, other, weight));
                }
            }
        }
    }
    for (a, b, weight) in edges_to_add {
        graph.update_edge(a, b, weight);
    }

    graph
}
